﻿
namespace Mastery_Tracker_for_LOL
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BaseStripMenu = new System.Windows.Forms.MenuStrip();
            this.strip_FileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.strip_NewSummoner = new System.Windows.Forms.ToolStripMenuItem();
            this.strip_SaveSummoner = new System.Windows.Forms.ToolStripMenuItem();
            this.strip_FileMenuSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.strip_Import = new System.Windows.Forms.ToolStripMenuItem();
            this.strip_Export = new System.Windows.Forms.ToolStripMenuItem();
            this.strip_FileMenuSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.strip_DeleteSummoner = new System.Windows.Forms.ToolStripMenuItem();
            this.strip_Close = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addAChampionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.strip_HelpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.strip_LanguageMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.strip_LanguagePicker = new System.Windows.Forms.ToolStripComboBox();
            this.strip_Help = new System.Windows.Forms.ToolStripMenuItem();
            this.group_SummonerStats = new System.Windows.Forms.GroupBox();
            this.panel_SummonerStats = new System.Windows.Forms.Panel();
            this.label_WinrateIndicator = new System.Windows.Forms.Label();
            this.panel_StatsAverageIndicator = new System.Windows.Forms.Panel();
            this.label_TotalGamesAverageIndicator = new System.Windows.Forms.Label();
            this.label_LostGamesAverageIndicator = new System.Windows.Forms.Label();
            this.label_WonGamesAverageIndicator = new System.Windows.Forms.Label();
            this.label_AverageColumn = new System.Windows.Forms.Label();
            this.panel_StatsName = new System.Windows.Forms.Panel();
            this.label_Winrate = new System.Windows.Forms.Label();
            this.label_TotalGames = new System.Windows.Forms.Label();
            this.label_LostGames = new System.Windows.Forms.Label();
            this.label_WonGames = new System.Windows.Forms.Label();
            this.label_MatchesColumn = new System.Windows.Forms.Label();
            this.panel_StatsAmountIndicator = new System.Windows.Forms.Panel();
            this.label_TotalGamesIndicator = new System.Windows.Forms.Label();
            this.label_LostGamesIndicator = new System.Windows.Forms.Label();
            this.label_WonGamesIndicator = new System.Windows.Forms.Label();
            this.label_AmountColumn = new System.Windows.Forms.Label();
            this.panel_KDA = new System.Windows.Forms.Panel();
            this.label_KDAIndicator = new System.Windows.Forms.Label();
            this.label_KDA = new System.Windows.Forms.Label();
            this.groupBox_ChampStats = new System.Windows.Forms.GroupBox();
            this.panel_ActionButtons = new System.Windows.Forms.Panel();
            this.panel_ChampMatches = new System.Windows.Forms.Panel();
            this.label_ChampWRIndicator = new System.Windows.Forms.Label();
            this.label_ChampWR = new System.Windows.Forms.Label();
            this.label_ChampWonMatchesIndicator = new System.Windows.Forms.Label();
            this.label_ChampWonMatches = new System.Windows.Forms.Label();
            this.label_ChampMatchesIndicator = new System.Windows.Forms.Label();
            this.label_ChampMatches = new System.Windows.Forms.Label();
            this.button_ViewMatches = new System.Windows.Forms.Button();
            this.button_AddMatch = new System.Windows.Forms.Button();
            this.panel_ChampRemainingData = new System.Windows.Forms.Panel();
            this.panel_RemainingLostGames = new System.Windows.Forms.Panel();
            this.label_RemainingLostGamesIndicator = new System.Windows.Forms.Label();
            this.label_RemainingLostGames = new System.Windows.Forms.Label();
            this.panel_RemainingWonGames = new System.Windows.Forms.Panel();
            this.label_RemainingWonGamesIndicator = new System.Windows.Forms.Label();
            this.label_RemainingWonGames = new System.Windows.Forms.Label();
            this.panel_RemainingAvgGames = new System.Windows.Forms.Panel();
            this.label_RemainingAvgGamesIndicator = new System.Windows.Forms.Label();
            this.label_RemainingAvgGames = new System.Windows.Forms.Label();
            this.panel_RemainingCP = new System.Windows.Forms.Panel();
            this.label_RemainingCPIndicator = new System.Windows.Forms.Label();
            this.label_RemainingCP = new System.Windows.Forms.Label();
            this.label_RemainingTitle = new System.Windows.Forms.Label();
            this.panel_PickAChamp = new System.Windows.Forms.Panel();
            this.label_CurrentPointsIndicator = new System.Windows.Forms.Label();
            this.label_CurrentPoints = new System.Windows.Forms.Label();
            this.checkBox_M6 = new System.Windows.Forms.CheckBox();
            this.checkBox_M7 = new System.Windows.Forms.CheckBox();
            this.label_MasteryLevelIndicator = new System.Windows.Forms.Label();
            this.label_MasteryLevel = new System.Windows.Forms.Label();
            this.combo_PickAChamp = new System.Windows.Forms.ComboBox();
            this.championsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mainDBDataSet = new Mastery_Tracker_for_LOL.MainDBDataSet();
            this.toolTip_PickAChamp = new System.Windows.Forms.ToolTip(this.components);
            this.championsTableAdapter = new Mastery_Tracker_for_LOL.MainDBDataSetTableAdapters.ChampionsTableAdapter();
            this.BaseStripMenu.SuspendLayout();
            this.group_SummonerStats.SuspendLayout();
            this.panel_SummonerStats.SuspendLayout();
            this.panel_StatsAverageIndicator.SuspendLayout();
            this.panel_StatsName.SuspendLayout();
            this.panel_StatsAmountIndicator.SuspendLayout();
            this.panel_KDA.SuspendLayout();
            this.groupBox_ChampStats.SuspendLayout();
            this.panel_ActionButtons.SuspendLayout();
            this.panel_ChampMatches.SuspendLayout();
            this.panel_ChampRemainingData.SuspendLayout();
            this.panel_RemainingLostGames.SuspendLayout();
            this.panel_RemainingWonGames.SuspendLayout();
            this.panel_RemainingAvgGames.SuspendLayout();
            this.panel_RemainingCP.SuspendLayout();
            this.panel_PickAChamp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.championsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDBDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // BaseStripMenu
            // 
            this.BaseStripMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.strip_FileMenu,
            this.editToolStripMenuItem,
            this.strip_HelpMenu});
            this.BaseStripMenu.Location = new System.Drawing.Point(0, 0);
            this.BaseStripMenu.Name = "BaseStripMenu";
            this.BaseStripMenu.Size = new System.Drawing.Size(465, 24);
            this.BaseStripMenu.TabIndex = 10;
            this.BaseStripMenu.Text = "menuStrip1";
            // 
            // strip_FileMenu
            // 
            this.strip_FileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.strip_NewSummoner,
            this.strip_SaveSummoner,
            this.strip_FileMenuSeparator1,
            this.strip_Import,
            this.strip_Export,
            this.strip_FileMenuSeparator2,
            this.strip_DeleteSummoner,
            this.strip_Close});
            this.strip_FileMenu.Name = "strip_FileMenu";
            this.strip_FileMenu.Size = new System.Drawing.Size(37, 20);
            this.strip_FileMenu.Text = "File";
            // 
            // strip_NewSummoner
            // 
            this.strip_NewSummoner.Name = "strip_NewSummoner";
            this.strip_NewSummoner.Size = new System.Drawing.Size(169, 22);
            this.strip_NewSummoner.Text = "New Summoner";
            this.strip_NewSummoner.Click += new System.EventHandler(this.strip_NewSummoner_Click);
            // 
            // strip_SaveSummoner
            // 
            this.strip_SaveSummoner.Name = "strip_SaveSummoner";
            this.strip_SaveSummoner.Size = new System.Drawing.Size(169, 22);
            this.strip_SaveSummoner.Text = "Save Summoner";
            // 
            // strip_FileMenuSeparator1
            // 
            this.strip_FileMenuSeparator1.Name = "strip_FileMenuSeparator1";
            this.strip_FileMenuSeparator1.Size = new System.Drawing.Size(166, 6);
            // 
            // strip_Import
            // 
            this.strip_Import.Name = "strip_Import";
            this.strip_Import.Size = new System.Drawing.Size(169, 22);
            this.strip_Import.Text = "Import ";
            // 
            // strip_Export
            // 
            this.strip_Export.Name = "strip_Export";
            this.strip_Export.Size = new System.Drawing.Size(169, 22);
            this.strip_Export.Text = "Export";
            // 
            // strip_FileMenuSeparator2
            // 
            this.strip_FileMenuSeparator2.Name = "strip_FileMenuSeparator2";
            this.strip_FileMenuSeparator2.Size = new System.Drawing.Size(166, 6);
            // 
            // strip_DeleteSummoner
            // 
            this.strip_DeleteSummoner.Name = "strip_DeleteSummoner";
            this.strip_DeleteSummoner.Size = new System.Drawing.Size(169, 22);
            this.strip_DeleteSummoner.Text = "Delete Summoner";
            // 
            // strip_Close
            // 
            this.strip_Close.Name = "strip_Close";
            this.strip_Close.Size = new System.Drawing.Size(169, 22);
            this.strip_Close.Text = "Close";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addAChampionToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // addAChampionToolStripMenuItem
            // 
            this.addAChampionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.addAChampionToolStripMenuItem.Name = "addAChampionToolStripMenuItem";
            this.addAChampionToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.addAChampionToolStripMenuItem.Text = "Champions";
            // 
            // addToolStripMenuItem
            // 
            this.addToolStripMenuItem.Name = "addToolStripMenuItem";
            this.addToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.addToolStripMenuItem.Text = "Add";
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            // 
            // strip_HelpMenu
            // 
            this.strip_HelpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.strip_LanguageMenu,
            this.strip_Help});
            this.strip_HelpMenu.Name = "strip_HelpMenu";
            this.strip_HelpMenu.Size = new System.Drawing.Size(44, 20);
            this.strip_HelpMenu.Text = "Help";
            // 
            // strip_LanguageMenu
            // 
            this.strip_LanguageMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.strip_LanguagePicker});
            this.strip_LanguageMenu.Name = "strip_LanguageMenu";
            this.strip_LanguageMenu.Size = new System.Drawing.Size(126, 22);
            this.strip_LanguageMenu.Text = "Language";
            // 
            // strip_LanguagePicker
            // 
            this.strip_LanguagePicker.Items.AddRange(new object[] {
            "System Default",
            "English",
            "Spanish"});
            this.strip_LanguagePicker.Name = "strip_LanguagePicker";
            this.strip_LanguagePicker.Size = new System.Drawing.Size(121, 23);
            // 
            // strip_Help
            // 
            this.strip_Help.Name = "strip_Help";
            this.strip_Help.Size = new System.Drawing.Size(126, 22);
            this.strip_Help.Text = "Help";
            // 
            // group_SummonerStats
            // 
            this.group_SummonerStats.CausesValidation = false;
            this.group_SummonerStats.Controls.Add(this.panel_SummonerStats);
            this.group_SummonerStats.Controls.Add(this.panel_KDA);
            this.group_SummonerStats.Font = new System.Drawing.Font("Friz Quadrata Std", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group_SummonerStats.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(210)))), ((int)(((byte)(139)))));
            this.group_SummonerStats.Location = new System.Drawing.Point(10, 33);
            this.group_SummonerStats.Margin = new System.Windows.Forms.Padding(12, 9, 12, 3);
            this.group_SummonerStats.Name = "group_SummonerStats";
            this.group_SummonerStats.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.group_SummonerStats.Size = new System.Drawing.Size(448, 191);
            this.group_SummonerStats.TabIndex = 10;
            this.group_SummonerStats.TabStop = false;
            this.group_SummonerStats.Text = "Summoner Stats";
            // 
            // panel_SummonerStats
            // 
            this.panel_SummonerStats.Controls.Add(this.label_WinrateIndicator);
            this.panel_SummonerStats.Controls.Add(this.panel_StatsAverageIndicator);
            this.panel_SummonerStats.Controls.Add(this.panel_StatsName);
            this.panel_SummonerStats.Controls.Add(this.panel_StatsAmountIndicator);
            this.panel_SummonerStats.Location = new System.Drawing.Point(6, 60);
            this.panel_SummonerStats.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.panel_SummonerStats.Name = "panel_SummonerStats";
            this.panel_SummonerStats.Size = new System.Drawing.Size(436, 126);
            this.panel_SummonerStats.TabIndex = 10;
            // 
            // label_WinrateIndicator
            // 
            this.label_WinrateIndicator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(117)))), ((int)(((byte)(174)))));
            this.label_WinrateIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_WinrateIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_WinrateIndicator.ForeColor = System.Drawing.Color.White;
            this.label_WinrateIndicator.Location = new System.Drawing.Point(146, 99);
            this.label_WinrateIndicator.Name = "label_WinrateIndicator";
            this.label_WinrateIndicator.Size = new System.Drawing.Size(286, 24);
            this.label_WinrateIndicator.TabIndex = 10;
            this.label_WinrateIndicator.Text = "0";
            this.label_WinrateIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_StatsAverageIndicator
            // 
            this.panel_StatsAverageIndicator.Controls.Add(this.label_TotalGamesAverageIndicator);
            this.panel_StatsAverageIndicator.Controls.Add(this.label_LostGamesAverageIndicator);
            this.panel_StatsAverageIndicator.Controls.Add(this.label_WonGamesAverageIndicator);
            this.panel_StatsAverageIndicator.Controls.Add(this.label_AverageColumn);
            this.panel_StatsAverageIndicator.Location = new System.Drawing.Point(289, 3);
            this.panel_StatsAverageIndicator.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.panel_StatsAverageIndicator.Name = "panel_StatsAverageIndicator";
            this.panel_StatsAverageIndicator.Size = new System.Drawing.Size(143, 96);
            this.panel_StatsAverageIndicator.TabIndex = 10;
            // 
            // label_TotalGamesAverageIndicator
            // 
            this.label_TotalGamesAverageIndicator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(117)))), ((int)(((byte)(174)))));
            this.label_TotalGamesAverageIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_TotalGamesAverageIndicator.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_TotalGamesAverageIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_TotalGamesAverageIndicator.ForeColor = System.Drawing.Color.White;
            this.label_TotalGamesAverageIndicator.Location = new System.Drawing.Point(0, 72);
            this.label_TotalGamesAverageIndicator.Name = "label_TotalGamesAverageIndicator";
            this.label_TotalGamesAverageIndicator.Size = new System.Drawing.Size(143, 24);
            this.label_TotalGamesAverageIndicator.TabIndex = 10;
            this.label_TotalGamesAverageIndicator.Text = "0";
            this.label_TotalGamesAverageIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_LostGamesAverageIndicator
            // 
            this.label_LostGamesAverageIndicator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(117)))), ((int)(((byte)(174)))));
            this.label_LostGamesAverageIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_LostGamesAverageIndicator.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_LostGamesAverageIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_LostGamesAverageIndicator.ForeColor = System.Drawing.Color.White;
            this.label_LostGamesAverageIndicator.Location = new System.Drawing.Point(0, 48);
            this.label_LostGamesAverageIndicator.Name = "label_LostGamesAverageIndicator";
            this.label_LostGamesAverageIndicator.Size = new System.Drawing.Size(143, 24);
            this.label_LostGamesAverageIndicator.TabIndex = 10;
            this.label_LostGamesAverageIndicator.Text = "0";
            this.label_LostGamesAverageIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_WonGamesAverageIndicator
            // 
            this.label_WonGamesAverageIndicator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(117)))), ((int)(((byte)(174)))));
            this.label_WonGamesAverageIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_WonGamesAverageIndicator.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_WonGamesAverageIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_WonGamesAverageIndicator.ForeColor = System.Drawing.Color.White;
            this.label_WonGamesAverageIndicator.Location = new System.Drawing.Point(0, 24);
            this.label_WonGamesAverageIndicator.Name = "label_WonGamesAverageIndicator";
            this.label_WonGamesAverageIndicator.Size = new System.Drawing.Size(143, 24);
            this.label_WonGamesAverageIndicator.TabIndex = 10;
            this.label_WonGamesAverageIndicator.Text = "0";
            this.label_WonGamesAverageIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_AverageColumn
            // 
            this.label_AverageColumn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(153)))), ((int)(((byte)(51)))));
            this.label_AverageColumn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_AverageColumn.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_AverageColumn.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_AverageColumn.ForeColor = System.Drawing.Color.Black;
            this.label_AverageColumn.Location = new System.Drawing.Point(0, 0);
            this.label_AverageColumn.Name = "label_AverageColumn";
            this.label_AverageColumn.Size = new System.Drawing.Size(143, 24);
            this.label_AverageColumn.TabIndex = 10;
            this.label_AverageColumn.Text = "Average CP";
            this.label_AverageColumn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_StatsName
            // 
            this.panel_StatsName.Controls.Add(this.label_Winrate);
            this.panel_StatsName.Controls.Add(this.label_TotalGames);
            this.panel_StatsName.Controls.Add(this.label_LostGames);
            this.panel_StatsName.Controls.Add(this.label_WonGames);
            this.panel_StatsName.Controls.Add(this.label_MatchesColumn);
            this.panel_StatsName.Location = new System.Drawing.Point(3, 3);
            this.panel_StatsName.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.panel_StatsName.Name = "panel_StatsName";
            this.panel_StatsName.Size = new System.Drawing.Size(143, 120);
            this.panel_StatsName.TabIndex = 10;
            // 
            // label_Winrate
            // 
            this.label_Winrate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.label_Winrate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_Winrate.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_Winrate.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Winrate.Location = new System.Drawing.Point(0, 96);
            this.label_Winrate.Name = "label_Winrate";
            this.label_Winrate.Size = new System.Drawing.Size(143, 24);
            this.label_Winrate.TabIndex = 10;
            this.label_Winrate.Text = "Winrate";
            this.label_Winrate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_TotalGames
            // 
            this.label_TotalGames.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.label_TotalGames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_TotalGames.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_TotalGames.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_TotalGames.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(160)))), ((int)(((byte)(155)))), ((int)(((byte)(140)))));
            this.label_TotalGames.Location = new System.Drawing.Point(0, 72);
            this.label_TotalGames.Name = "label_TotalGames";
            this.label_TotalGames.Size = new System.Drawing.Size(143, 24);
            this.label_TotalGames.TabIndex = 10;
            this.label_TotalGames.Text = "Total";
            this.label_TotalGames.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_LostGames
            // 
            this.label_LostGames.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.label_LostGames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_LostGames.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_LostGames.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_LostGames.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(33)))), ((int)(((byte)(65)))));
            this.label_LostGames.Location = new System.Drawing.Point(0, 48);
            this.label_LostGames.Name = "label_LostGames";
            this.label_LostGames.Size = new System.Drawing.Size(143, 24);
            this.label_LostGames.TabIndex = 10;
            this.label_LostGames.Text = "Lost";
            this.label_LostGames.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_WonGames
            // 
            this.label_WonGames.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.label_WonGames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_WonGames.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_WonGames.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_WonGames.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(196)))), ((int)(((byte)(237)))));
            this.label_WonGames.Location = new System.Drawing.Point(0, 24);
            this.label_WonGames.Name = "label_WonGames";
            this.label_WonGames.Size = new System.Drawing.Size(143, 24);
            this.label_WonGames.TabIndex = 10;
            this.label_WonGames.Text = "Won";
            this.label_WonGames.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_MatchesColumn
            // 
            this.label_MatchesColumn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(153)))), ((int)(((byte)(51)))));
            this.label_MatchesColumn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_MatchesColumn.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_MatchesColumn.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MatchesColumn.ForeColor = System.Drawing.Color.Black;
            this.label_MatchesColumn.Location = new System.Drawing.Point(0, 0);
            this.label_MatchesColumn.Name = "label_MatchesColumn";
            this.label_MatchesColumn.Size = new System.Drawing.Size(143, 24);
            this.label_MatchesColumn.TabIndex = 10;
            this.label_MatchesColumn.Text = "Matches";
            this.label_MatchesColumn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_StatsAmountIndicator
            // 
            this.panel_StatsAmountIndicator.Controls.Add(this.label_TotalGamesIndicator);
            this.panel_StatsAmountIndicator.Controls.Add(this.label_LostGamesIndicator);
            this.panel_StatsAmountIndicator.Controls.Add(this.label_WonGamesIndicator);
            this.panel_StatsAmountIndicator.Controls.Add(this.label_AmountColumn);
            this.panel_StatsAmountIndicator.Location = new System.Drawing.Point(146, 3);
            this.panel_StatsAmountIndicator.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.panel_StatsAmountIndicator.Name = "panel_StatsAmountIndicator";
            this.panel_StatsAmountIndicator.Size = new System.Drawing.Size(143, 96);
            this.panel_StatsAmountIndicator.TabIndex = 10;
            // 
            // label_TotalGamesIndicator
            // 
            this.label_TotalGamesIndicator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(117)))), ((int)(((byte)(174)))));
            this.label_TotalGamesIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_TotalGamesIndicator.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_TotalGamesIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_TotalGamesIndicator.ForeColor = System.Drawing.Color.White;
            this.label_TotalGamesIndicator.Location = new System.Drawing.Point(0, 72);
            this.label_TotalGamesIndicator.Name = "label_TotalGamesIndicator";
            this.label_TotalGamesIndicator.Size = new System.Drawing.Size(143, 24);
            this.label_TotalGamesIndicator.TabIndex = 10;
            this.label_TotalGamesIndicator.Text = "0";
            this.label_TotalGamesIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_LostGamesIndicator
            // 
            this.label_LostGamesIndicator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(117)))), ((int)(((byte)(174)))));
            this.label_LostGamesIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_LostGamesIndicator.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_LostGamesIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_LostGamesIndicator.ForeColor = System.Drawing.Color.White;
            this.label_LostGamesIndicator.Location = new System.Drawing.Point(0, 48);
            this.label_LostGamesIndicator.Name = "label_LostGamesIndicator";
            this.label_LostGamesIndicator.Size = new System.Drawing.Size(143, 24);
            this.label_LostGamesIndicator.TabIndex = 10;
            this.label_LostGamesIndicator.Text = "0";
            this.label_LostGamesIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_WonGamesIndicator
            // 
            this.label_WonGamesIndicator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(117)))), ((int)(((byte)(174)))));
            this.label_WonGamesIndicator.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_WonGamesIndicator.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_WonGamesIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_WonGamesIndicator.ForeColor = System.Drawing.Color.White;
            this.label_WonGamesIndicator.Location = new System.Drawing.Point(0, 24);
            this.label_WonGamesIndicator.Name = "label_WonGamesIndicator";
            this.label_WonGamesIndicator.Size = new System.Drawing.Size(143, 24);
            this.label_WonGamesIndicator.TabIndex = 10;
            this.label_WonGamesIndicator.Text = "0";
            this.label_WonGamesIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_AmountColumn
            // 
            this.label_AmountColumn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(153)))), ((int)(((byte)(51)))));
            this.label_AmountColumn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_AmountColumn.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_AmountColumn.Font = new System.Drawing.Font("Friz Quadrata Std", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_AmountColumn.ForeColor = System.Drawing.Color.Black;
            this.label_AmountColumn.Location = new System.Drawing.Point(0, 0);
            this.label_AmountColumn.Name = "label_AmountColumn";
            this.label_AmountColumn.Size = new System.Drawing.Size(143, 24);
            this.label_AmountColumn.TabIndex = 10;
            this.label_AmountColumn.Text = "Amount";
            this.label_AmountColumn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel_KDA
            // 
            this.panel_KDA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(40)))));
            this.panel_KDA.Controls.Add(this.label_KDAIndicator);
            this.panel_KDA.Controls.Add(this.label_KDA);
            this.panel_KDA.Location = new System.Drawing.Point(6, 27);
            this.panel_KDA.Name = "panel_KDA";
            this.panel_KDA.Size = new System.Drawing.Size(436, 28);
            this.panel_KDA.TabIndex = 10;
            // 
            // label_KDAIndicator
            // 
            this.label_KDAIndicator.AutoSize = true;
            this.label_KDAIndicator.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label_KDAIndicator.Location = new System.Drawing.Point(243, 3);
            this.label_KDAIndicator.Name = "label_KDAIndicator";
            this.label_KDAIndicator.Size = new System.Drawing.Size(193, 22);
            this.label_KDAIndicator.TabIndex = 10;
            this.label_KDAIndicator.Text = "00,00  ( 00 / 00 / 00 )";
            this.label_KDAIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_KDA
            // 
            this.label_KDA.AutoSize = true;
            this.label_KDA.Location = new System.Drawing.Point(3, 3);
            this.label_KDA.Name = "label_KDA";
            this.label_KDA.Size = new System.Drawing.Size(127, 22);
            this.label_KDA.TabIndex = 10;
            this.label_KDA.Text = "Average KDA:";
            this.label_KDA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox_ChampStats
            // 
            this.groupBox_ChampStats.CausesValidation = false;
            this.groupBox_ChampStats.Controls.Add(this.panel_ActionButtons);
            this.groupBox_ChampStats.Controls.Add(this.panel_ChampRemainingData);
            this.groupBox_ChampStats.Controls.Add(this.panel_PickAChamp);
            this.groupBox_ChampStats.Font = new System.Drawing.Font("Friz Quadrata Std", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox_ChampStats.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(210)))), ((int)(((byte)(139)))));
            this.groupBox_ChampStats.Location = new System.Drawing.Point(10, 230);
            this.groupBox_ChampStats.Margin = new System.Windows.Forms.Padding(3, 3, 12, 3);
            this.groupBox_ChampStats.Name = "groupBox_ChampStats";
            this.groupBox_ChampStats.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.groupBox_ChampStats.Size = new System.Drawing.Size(448, 229);
            this.groupBox_ChampStats.TabIndex = 10;
            this.groupBox_ChampStats.TabStop = false;
            this.groupBox_ChampStats.Text = "Champions Stats";
            // 
            // panel_ActionButtons
            // 
            this.panel_ActionButtons.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(40)))));
            this.panel_ActionButtons.Controls.Add(this.panel_ChampMatches);
            this.panel_ActionButtons.Controls.Add(this.button_ViewMatches);
            this.panel_ActionButtons.Controls.Add(this.button_AddMatch);
            this.panel_ActionButtons.Location = new System.Drawing.Point(6, 84);
            this.panel_ActionButtons.Name = "panel_ActionButtons";
            this.panel_ActionButtons.Padding = new System.Windows.Forms.Padding(3);
            this.panel_ActionButtons.Size = new System.Drawing.Size(149, 140);
            this.panel_ActionButtons.TabIndex = 10;
            // 
            // panel_ChampMatches
            // 
            this.panel_ChampMatches.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_ChampMatches.Controls.Add(this.label_ChampWRIndicator);
            this.panel_ChampMatches.Controls.Add(this.label_ChampWR);
            this.panel_ChampMatches.Controls.Add(this.label_ChampWonMatchesIndicator);
            this.panel_ChampMatches.Controls.Add(this.label_ChampWonMatches);
            this.panel_ChampMatches.Controls.Add(this.label_ChampMatchesIndicator);
            this.panel_ChampMatches.Controls.Add(this.label_ChampMatches);
            this.panel_ChampMatches.Location = new System.Drawing.Point(5, 6);
            this.panel_ChampMatches.Name = "panel_ChampMatches";
            this.panel_ChampMatches.Size = new System.Drawing.Size(144, 57);
            this.panel_ChampMatches.TabIndex = 10;
            // 
            // label_ChampWRIndicator
            // 
            this.label_ChampWRIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_ChampWRIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ChampWRIndicator.Location = new System.Drawing.Point(77, 36);
            this.label_ChampWRIndicator.Margin = new System.Windows.Forms.Padding(3);
            this.label_ChampWRIndicator.Name = "label_ChampWRIndicator";
            this.label_ChampWRIndicator.Size = new System.Drawing.Size(67, 18);
            this.label_ChampWRIndicator.TabIndex = 10;
            this.label_ChampWRIndicator.Text = "0,00";
            this.label_ChampWRIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_ChampWR
            // 
            this.label_ChampWR.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ChampWR.Location = new System.Drawing.Point(-3, 36);
            this.label_ChampWR.Margin = new System.Windows.Forms.Padding(3);
            this.label_ChampWR.Name = "label_ChampWR";
            this.label_ChampWR.Size = new System.Drawing.Size(73, 18);
            this.label_ChampWR.TabIndex = 10;
            this.label_ChampWR.Text = "Winrate:";
            this.label_ChampWR.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_ChampWonMatchesIndicator
            // 
            this.label_ChampWonMatchesIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_ChampWonMatchesIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ChampWonMatchesIndicator.Location = new System.Drawing.Point(77, 18);
            this.label_ChampWonMatchesIndicator.Margin = new System.Windows.Forms.Padding(0);
            this.label_ChampWonMatchesIndicator.Name = "label_ChampWonMatchesIndicator";
            this.label_ChampWonMatchesIndicator.Size = new System.Drawing.Size(67, 18);
            this.label_ChampWonMatchesIndicator.TabIndex = 10;
            this.label_ChampWonMatchesIndicator.Text = "00000";
            this.label_ChampWonMatchesIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_ChampWonMatches
            // 
            this.label_ChampWonMatches.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ChampWonMatches.Location = new System.Drawing.Point(-3, 18);
            this.label_ChampWonMatches.Margin = new System.Windows.Forms.Padding(0);
            this.label_ChampWonMatches.Name = "label_ChampWonMatches";
            this.label_ChampWonMatches.Size = new System.Drawing.Size(73, 18);
            this.label_ChampWonMatches.TabIndex = 10;
            this.label_ChampWonMatches.Text = "Won:";
            this.label_ChampWonMatches.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_ChampMatchesIndicator
            // 
            this.label_ChampMatchesIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_ChampMatchesIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ChampMatchesIndicator.Location = new System.Drawing.Point(77, 0);
            this.label_ChampMatchesIndicator.Margin = new System.Windows.Forms.Padding(3);
            this.label_ChampMatchesIndicator.Name = "label_ChampMatchesIndicator";
            this.label_ChampMatchesIndicator.Size = new System.Drawing.Size(67, 18);
            this.label_ChampMatchesIndicator.TabIndex = 10;
            this.label_ChampMatchesIndicator.Text = "00000";
            this.label_ChampMatchesIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_ChampMatches
            // 
            this.label_ChampMatches.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ChampMatches.Location = new System.Drawing.Point(-3, 0);
            this.label_ChampMatches.Margin = new System.Windows.Forms.Padding(3);
            this.label_ChampMatches.Name = "label_ChampMatches";
            this.label_ChampMatches.Size = new System.Drawing.Size(73, 18);
            this.label_ChampMatches.TabIndex = 10;
            this.label_ChampMatches.Text = "Matches:";
            this.label_ChampMatches.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button_ViewMatches
            // 
            this.button_ViewMatches.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.button_ViewMatches.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(196)))), ((int)(((byte)(237)))));
            this.button_ViewMatches.FlatAppearance.BorderSize = 2;
            this.button_ViewMatches.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ViewMatches.Font = new System.Drawing.Font("Friz Quadrata Std", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ViewMatches.Location = new System.Drawing.Point(11, 69);
            this.button_ViewMatches.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.button_ViewMatches.Name = "button_ViewMatches";
            this.button_ViewMatches.Size = new System.Drawing.Size(126, 28);
            this.button_ViewMatches.TabIndex = 3;
            this.button_ViewMatches.Text = "Matches History";
            this.toolTip_PickAChamp.SetToolTip(this.button_ViewMatches, "See all the games played with the champion");
            this.button_ViewMatches.UseVisualStyleBackColor = false;
            // 
            // button_AddMatch
            // 
            this.button_AddMatch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.button_AddMatch.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(196)))), ((int)(((byte)(237)))));
            this.button_AddMatch.FlatAppearance.BorderSize = 2;
            this.button_AddMatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_AddMatch.Font = new System.Drawing.Font("Friz Quadrata Std", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_AddMatch.Location = new System.Drawing.Point(11, 103);
            this.button_AddMatch.Margin = new System.Windows.Forms.Padding(0, 0, 0, 3);
            this.button_AddMatch.Name = "button_AddMatch";
            this.button_AddMatch.Size = new System.Drawing.Size(126, 28);
            this.button_AddMatch.TabIndex = 4;
            this.button_AddMatch.Text = "Add Match";
            this.toolTip_PickAChamp.SetToolTip(this.button_AddMatch, "Add new game data for the champion");
            this.button_AddMatch.UseVisualStyleBackColor = false;
            // 
            // panel_ChampRemainingData
            // 
            this.panel_ChampRemainingData.AutoSize = true;
            this.panel_ChampRemainingData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(40)))));
            this.panel_ChampRemainingData.Controls.Add(this.panel_RemainingLostGames);
            this.panel_ChampRemainingData.Controls.Add(this.panel_RemainingWonGames);
            this.panel_ChampRemainingData.Controls.Add(this.panel_RemainingAvgGames);
            this.panel_ChampRemainingData.Controls.Add(this.panel_RemainingCP);
            this.panel_ChampRemainingData.Controls.Add(this.label_RemainingTitle);
            this.panel_ChampRemainingData.Location = new System.Drawing.Point(161, 84);
            this.panel_ChampRemainingData.Name = "panel_ChampRemainingData";
            this.panel_ChampRemainingData.Size = new System.Drawing.Size(281, 139);
            this.panel_ChampRemainingData.TabIndex = 10;
            // 
            // panel_RemainingLostGames
            // 
            this.panel_RemainingLostGames.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_RemainingLostGames.Controls.Add(this.label_RemainingLostGamesIndicator);
            this.panel_RemainingLostGames.Controls.Add(this.label_RemainingLostGames);
            this.panel_RemainingLostGames.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_RemainingLostGames.Location = new System.Drawing.Point(0, 100);
            this.panel_RemainingLostGames.Name = "panel_RemainingLostGames";
            this.panel_RemainingLostGames.Size = new System.Drawing.Size(281, 26);
            this.panel_RemainingLostGames.TabIndex = 10;
            // 
            // label_RemainingLostGamesIndicator
            // 
            this.label_RemainingLostGamesIndicator.AutoSize = true;
            this.label_RemainingLostGamesIndicator.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_RemainingLostGamesIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_RemainingLostGamesIndicator.Location = new System.Drawing.Point(246, 0);
            this.label_RemainingLostGamesIndicator.Margin = new System.Windows.Forms.Padding(3);
            this.label_RemainingLostGamesIndicator.Name = "label_RemainingLostGamesIndicator";
            this.label_RemainingLostGamesIndicator.Size = new System.Drawing.Size(35, 18);
            this.label_RemainingLostGamesIndicator.TabIndex = 10;
            this.label_RemainingLostGamesIndicator.Text = "000";
            this.label_RemainingLostGamesIndicator.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_RemainingLostGames
            // 
            this.label_RemainingLostGames.AutoSize = true;
            this.label_RemainingLostGames.Dock = System.Windows.Forms.DockStyle.Left;
            this.label_RemainingLostGames.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_RemainingLostGames.Location = new System.Drawing.Point(0, 0);
            this.label_RemainingLostGames.Margin = new System.Windows.Forms.Padding(3);
            this.label_RemainingLostGames.Name = "label_RemainingLostGames";
            this.label_RemainingLostGames.Size = new System.Drawing.Size(93, 18);
            this.label_RemainingLostGames.TabIndex = 10;
            this.label_RemainingLostGames.Text = "Lost Games:";
            this.label_RemainingLostGames.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel_RemainingWonGames
            // 
            this.panel_RemainingWonGames.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_RemainingWonGames.Controls.Add(this.label_RemainingWonGamesIndicator);
            this.panel_RemainingWonGames.Controls.Add(this.label_RemainingWonGames);
            this.panel_RemainingWonGames.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_RemainingWonGames.Location = new System.Drawing.Point(0, 74);
            this.panel_RemainingWonGames.Name = "panel_RemainingWonGames";
            this.panel_RemainingWonGames.Size = new System.Drawing.Size(281, 26);
            this.panel_RemainingWonGames.TabIndex = 10;
            // 
            // label_RemainingWonGamesIndicator
            // 
            this.label_RemainingWonGamesIndicator.AutoSize = true;
            this.label_RemainingWonGamesIndicator.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_RemainingWonGamesIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_RemainingWonGamesIndicator.Location = new System.Drawing.Point(246, 0);
            this.label_RemainingWonGamesIndicator.Margin = new System.Windows.Forms.Padding(3);
            this.label_RemainingWonGamesIndicator.Name = "label_RemainingWonGamesIndicator";
            this.label_RemainingWonGamesIndicator.Size = new System.Drawing.Size(35, 18);
            this.label_RemainingWonGamesIndicator.TabIndex = 10;
            this.label_RemainingWonGamesIndicator.Text = "000";
            this.label_RemainingWonGamesIndicator.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_RemainingWonGames
            // 
            this.label_RemainingWonGames.AutoSize = true;
            this.label_RemainingWonGames.Dock = System.Windows.Forms.DockStyle.Left;
            this.label_RemainingWonGames.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_RemainingWonGames.Location = new System.Drawing.Point(0, 0);
            this.label_RemainingWonGames.Margin = new System.Windows.Forms.Padding(3);
            this.label_RemainingWonGames.Name = "label_RemainingWonGames";
            this.label_RemainingWonGames.Size = new System.Drawing.Size(118, 18);
            this.label_RemainingWonGames.TabIndex = 10;
            this.label_RemainingWonGames.Text = "Winned Games:";
            this.label_RemainingWonGames.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel_RemainingAvgGames
            // 
            this.panel_RemainingAvgGames.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_RemainingAvgGames.Controls.Add(this.label_RemainingAvgGamesIndicator);
            this.panel_RemainingAvgGames.Controls.Add(this.label_RemainingAvgGames);
            this.panel_RemainingAvgGames.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_RemainingAvgGames.Location = new System.Drawing.Point(0, 48);
            this.panel_RemainingAvgGames.Name = "panel_RemainingAvgGames";
            this.panel_RemainingAvgGames.Size = new System.Drawing.Size(281, 26);
            this.panel_RemainingAvgGames.TabIndex = 10;
            // 
            // label_RemainingAvgGamesIndicator
            // 
            this.label_RemainingAvgGamesIndicator.AutoSize = true;
            this.label_RemainingAvgGamesIndicator.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_RemainingAvgGamesIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_RemainingAvgGamesIndicator.Location = new System.Drawing.Point(246, 0);
            this.label_RemainingAvgGamesIndicator.Margin = new System.Windows.Forms.Padding(3);
            this.label_RemainingAvgGamesIndicator.Name = "label_RemainingAvgGamesIndicator";
            this.label_RemainingAvgGamesIndicator.Size = new System.Drawing.Size(35, 18);
            this.label_RemainingAvgGamesIndicator.TabIndex = 10;
            this.label_RemainingAvgGamesIndicator.Text = "000";
            this.label_RemainingAvgGamesIndicator.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_RemainingAvgGames
            // 
            this.label_RemainingAvgGames.AutoSize = true;
            this.label_RemainingAvgGames.Dock = System.Windows.Forms.DockStyle.Left;
            this.label_RemainingAvgGames.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_RemainingAvgGames.Location = new System.Drawing.Point(0, 0);
            this.label_RemainingAvgGames.Margin = new System.Windows.Forms.Padding(3);
            this.label_RemainingAvgGames.Name = "label_RemainingAvgGames";
            this.label_RemainingAvgGames.Size = new System.Drawing.Size(121, 18);
            this.label_RemainingAvgGames.TabIndex = 10;
            this.label_RemainingAvgGames.Text = "Average Games:";
            this.label_RemainingAvgGames.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel_RemainingCP
            // 
            this.panel_RemainingCP.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_RemainingCP.Controls.Add(this.label_RemainingCPIndicator);
            this.panel_RemainingCP.Controls.Add(this.label_RemainingCP);
            this.panel_RemainingCP.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_RemainingCP.Location = new System.Drawing.Point(0, 22);
            this.panel_RemainingCP.Name = "panel_RemainingCP";
            this.panel_RemainingCP.Size = new System.Drawing.Size(281, 26);
            this.panel_RemainingCP.TabIndex = 10;
            // 
            // label_RemainingCPIndicator
            // 
            this.label_RemainingCPIndicator.AutoSize = true;
            this.label_RemainingCPIndicator.Dock = System.Windows.Forms.DockStyle.Right;
            this.label_RemainingCPIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_RemainingCPIndicator.Location = new System.Drawing.Point(228, 0);
            this.label_RemainingCPIndicator.Margin = new System.Windows.Forms.Padding(3);
            this.label_RemainingCPIndicator.Name = "label_RemainingCPIndicator";
            this.label_RemainingCPIndicator.Size = new System.Drawing.Size(53, 18);
            this.label_RemainingCPIndicator.TabIndex = 10;
            this.label_RemainingCPIndicator.Text = "00000";
            this.label_RemainingCPIndicator.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_RemainingCP
            // 
            this.label_RemainingCP.AutoSize = true;
            this.label_RemainingCP.Dock = System.Windows.Forms.DockStyle.Left;
            this.label_RemainingCP.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_RemainingCP.Location = new System.Drawing.Point(0, 0);
            this.label_RemainingCP.Margin = new System.Windows.Forms.Padding(3);
            this.label_RemainingCP.Name = "label_RemainingCP";
            this.label_RemainingCP.Size = new System.Drawing.Size(130, 18);
            this.label_RemainingCP.TabIndex = 10;
            this.label_RemainingCP.Text = "Champion Points:";
            this.label_RemainingCP.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label_RemainingTitle
            // 
            this.label_RemainingTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_RemainingTitle.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_RemainingTitle.Location = new System.Drawing.Point(0, 0);
            this.label_RemainingTitle.Name = "label_RemainingTitle";
            this.label_RemainingTitle.Size = new System.Drawing.Size(281, 22);
            this.label_RemainingTitle.TabIndex = 10;
            this.label_RemainingTitle.Text = "Remaining to M5:";
            this.label_RemainingTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel_PickAChamp
            // 
            this.panel_PickAChamp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(40)))));
            this.panel_PickAChamp.Controls.Add(this.label_CurrentPointsIndicator);
            this.panel_PickAChamp.Controls.Add(this.label_CurrentPoints);
            this.panel_PickAChamp.Controls.Add(this.checkBox_M6);
            this.panel_PickAChamp.Controls.Add(this.checkBox_M7);
            this.panel_PickAChamp.Controls.Add(this.label_MasteryLevelIndicator);
            this.panel_PickAChamp.Controls.Add(this.label_MasteryLevel);
            this.panel_PickAChamp.Controls.Add(this.combo_PickAChamp);
            this.panel_PickAChamp.Location = new System.Drawing.Point(6, 29);
            this.panel_PickAChamp.Name = "panel_PickAChamp";
            this.panel_PickAChamp.Size = new System.Drawing.Size(436, 49);
            this.panel_PickAChamp.TabIndex = 10;
            // 
            // label_CurrentPointsIndicator
            // 
            this.label_CurrentPointsIndicator.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_CurrentPointsIndicator.AutoSize = true;
            this.label_CurrentPointsIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CurrentPointsIndicator.Location = new System.Drawing.Point(267, 28);
            this.label_CurrentPointsIndicator.Margin = new System.Windows.Forms.Padding(3);
            this.label_CurrentPointsIndicator.Name = "label_CurrentPointsIndicator";
            this.label_CurrentPointsIndicator.Size = new System.Drawing.Size(53, 18);
            this.label_CurrentPointsIndicator.TabIndex = 10;
            this.label_CurrentPointsIndicator.Text = "20000";
            // 
            // label_CurrentPoints
            // 
            this.label_CurrentPoints.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_CurrentPoints.Location = new System.Drawing.Point(152, 28);
            this.label_CurrentPoints.Margin = new System.Windows.Forms.Padding(3);
            this.label_CurrentPoints.Name = "label_CurrentPoints";
            this.label_CurrentPoints.Size = new System.Drawing.Size(109, 18);
            this.label_CurrentPoints.TabIndex = 10;
            this.label_CurrentPoints.Text = "Current Points:";
            // 
            // checkBox_M6
            // 
            this.checkBox_M6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_M6.AutoSize = true;
            this.checkBox_M6.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_M6.Location = new System.Drawing.Point(330, 3);
            this.checkBox_M6.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.checkBox_M6.Name = "checkBox_M6";
            this.checkBox_M6.Size = new System.Drawing.Size(50, 22);
            this.checkBox_M6.TabIndex = 1;
            this.checkBox_M6.Text = "M6";
            this.toolTip_PickAChamp.SetToolTip(this.checkBox_M6, "Check if M6 is already unlocked");
            this.checkBox_M6.UseVisualStyleBackColor = true;
            this.checkBox_M6.Visible = false;
            // 
            // checkBox_M7
            // 
            this.checkBox_M7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_M7.AutoSize = true;
            this.checkBox_M7.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox_M7.Location = new System.Drawing.Point(386, 3);
            this.checkBox_M7.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.checkBox_M7.Name = "checkBox_M7";
            this.checkBox_M7.Size = new System.Drawing.Size(50, 22);
            this.checkBox_M7.TabIndex = 2;
            this.checkBox_M7.Text = "M7";
            this.toolTip_PickAChamp.SetToolTip(this.checkBox_M7, "Check if M7 is already unlocked");
            this.checkBox_M7.UseVisualStyleBackColor = true;
            this.checkBox_M7.Visible = false;
            // 
            // label_MasteryLevelIndicator
            // 
            this.label_MasteryLevelIndicator.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label_MasteryLevelIndicator.AutoSize = true;
            this.label_MasteryLevelIndicator.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MasteryLevelIndicator.Location = new System.Drawing.Point(267, 4);
            this.label_MasteryLevelIndicator.Margin = new System.Windows.Forms.Padding(3);
            this.label_MasteryLevelIndicator.Name = "label_MasteryLevelIndicator";
            this.label_MasteryLevelIndicator.Size = new System.Drawing.Size(17, 18);
            this.label_MasteryLevelIndicator.TabIndex = 10;
            this.label_MasteryLevelIndicator.Text = "5";
            this.label_MasteryLevelIndicator.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label_MasteryLevel
            // 
            this.label_MasteryLevel.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_MasteryLevel.Location = new System.Drawing.Point(152, 4);
            this.label_MasteryLevel.Margin = new System.Windows.Forms.Padding(3);
            this.label_MasteryLevel.Name = "label_MasteryLevel";
            this.label_MasteryLevel.Size = new System.Drawing.Size(109, 18);
            this.label_MasteryLevel.TabIndex = 10;
            this.label_MasteryLevel.Text = "Mastery Level:";
            this.label_MasteryLevel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // combo_PickAChamp
            // 
            this.combo_PickAChamp.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.combo_PickAChamp.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.combo_PickAChamp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.combo_PickAChamp.Cursor = System.Windows.Forms.Cursors.Default;
            this.combo_PickAChamp.DataSource = this.championsBindingSource;
            this.combo_PickAChamp.DisplayMember = "ENChampName";
            this.combo_PickAChamp.Font = new System.Drawing.Font("Friz Quadrata Std", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.combo_PickAChamp.ForeColor = System.Drawing.Color.White;
            this.combo_PickAChamp.FormattingEnabled = true;
            this.combo_PickAChamp.IntegralHeight = false;
            this.combo_PickAChamp.Location = new System.Drawing.Point(5, 13);
            this.combo_PickAChamp.Name = "combo_PickAChamp";
            this.combo_PickAChamp.Size = new System.Drawing.Size(140, 25);
            this.combo_PickAChamp.TabIndex = 0;
            this.toolTip_PickAChamp.SetToolTip(this.combo_PickAChamp, "Select a champion from the dropdown list.\r\nYou can also type in characters for an" +
        " easier search");
            this.combo_PickAChamp.ValueMember = "Id";
            this.combo_PickAChamp.SelectedIndexChanged += new System.EventHandler(this.combo_PickAChamp_SelectedIndexChanged);
            // 
            // championsBindingSource
            // 
            this.championsBindingSource.DataMember = "Champions";
            this.championsBindingSource.DataSource = this.mainDBDataSet;
            // 
            // mainDBDataSet
            // 
            this.mainDBDataSet.DataSetName = "MainDBDataSet";
            this.mainDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // championsTableAdapter
            // 
            this.championsTableAdapter.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.ClientSize = new System.Drawing.Size(465, 462);
            this.Controls.Add(this.groupBox_ChampStats);
            this.Controls.Add(this.group_SummonerStats);
            this.Controls.Add(this.BaseStripMenu);
            this.Font = new System.Drawing.Font("Friz Quadrata Std", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.BaseStripMenu;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(0, 0, 0, 9);
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Mastery Tracker for League of Legends - by Patvis";
            this.Load += new System.EventHandler(this.Form1_Load_1);
            this.BaseStripMenu.ResumeLayout(false);
            this.BaseStripMenu.PerformLayout();
            this.group_SummonerStats.ResumeLayout(false);
            this.panel_SummonerStats.ResumeLayout(false);
            this.panel_StatsAverageIndicator.ResumeLayout(false);
            this.panel_StatsName.ResumeLayout(false);
            this.panel_StatsAmountIndicator.ResumeLayout(false);
            this.panel_KDA.ResumeLayout(false);
            this.panel_KDA.PerformLayout();
            this.groupBox_ChampStats.ResumeLayout(false);
            this.groupBox_ChampStats.PerformLayout();
            this.panel_ActionButtons.ResumeLayout(false);
            this.panel_ChampMatches.ResumeLayout(false);
            this.panel_ChampRemainingData.ResumeLayout(false);
            this.panel_RemainingLostGames.ResumeLayout(false);
            this.panel_RemainingLostGames.PerformLayout();
            this.panel_RemainingWonGames.ResumeLayout(false);
            this.panel_RemainingWonGames.PerformLayout();
            this.panel_RemainingAvgGames.ResumeLayout(false);
            this.panel_RemainingAvgGames.PerformLayout();
            this.panel_RemainingCP.ResumeLayout(false);
            this.panel_RemainingCP.PerformLayout();
            this.panel_PickAChamp.ResumeLayout(false);
            this.panel_PickAChamp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.championsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainDBDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip BaseStripMenu;
        private System.Windows.Forms.ToolStripMenuItem strip_FileMenu;
        private System.Windows.Forms.ToolStripMenuItem strip_NewSummoner;
        private System.Windows.Forms.ToolStripMenuItem strip_SaveSummoner;
        private System.Windows.Forms.ToolStripSeparator strip_FileMenuSeparator1;
        private System.Windows.Forms.ToolStripMenuItem strip_Import;
        private System.Windows.Forms.ToolStripMenuItem strip_Export;
        private System.Windows.Forms.ToolStripSeparator strip_FileMenuSeparator2;
        private System.Windows.Forms.ToolStripMenuItem strip_Close;
        private System.Windows.Forms.ToolStripMenuItem strip_HelpMenu;
        private System.Windows.Forms.ToolStripMenuItem strip_LanguageMenu;
        private System.Windows.Forms.ToolStripComboBox strip_LanguagePicker;
        private System.Windows.Forms.ToolStripMenuItem strip_Help;
        private System.Windows.Forms.GroupBox group_SummonerStats;
        private System.Windows.Forms.Panel panel_KDA;
        private System.Windows.Forms.Label label_KDA;
        private System.Windows.Forms.Panel panel_StatsName;
        private System.Windows.Forms.Label label_Winrate;
        private System.Windows.Forms.Label label_TotalGames;
        private System.Windows.Forms.Label label_LostGames;
        private System.Windows.Forms.Label label_WonGames;
        private System.Windows.Forms.Panel panel_StatsAmountIndicator;
        private System.Windows.Forms.Label label_WinrateIndicator;
        private System.Windows.Forms.Label label_TotalGamesIndicator;
        private System.Windows.Forms.Label label_LostGamesIndicator;
        private System.Windows.Forms.Label label_WonGamesIndicator;
        private System.Windows.Forms.Panel panel_SummonerStats;
        private System.Windows.Forms.Label label_MatchesColumn;
        private System.Windows.Forms.Label label_AmountColumn;
        private System.Windows.Forms.Panel panel_StatsAverageIndicator;
        private System.Windows.Forms.Label label_TotalGamesAverageIndicator;
        private System.Windows.Forms.Label label_LostGamesAverageIndicator;
        private System.Windows.Forms.Label label_WonGamesAverageIndicator;
        private System.Windows.Forms.Label label_AverageColumn;
        private System.Windows.Forms.ToolStripMenuItem strip_DeleteSummoner;
        private System.Windows.Forms.GroupBox groupBox_ChampStats;
        private System.Windows.Forms.ToolTip toolTip_PickAChamp;
        private System.Windows.Forms.CheckBox checkBox_M7;
        private System.Windows.Forms.CheckBox checkBox_M6;
        private System.Windows.Forms.Panel panel_PickAChamp;
        private System.Windows.Forms.Panel panel_ChampRemainingData;
        private System.Windows.Forms.Label label_RemainingTitle;
        private System.Windows.Forms.Label label_CurrentPoints;
        private System.Windows.Forms.Label label_MasteryLevel;
        private System.Windows.Forms.Label label_RemainingCP;
        private System.Windows.Forms.Label label_RemainingCPIndicator;
        private System.Windows.Forms.Label label_CurrentPointsIndicator;
        private System.Windows.Forms.Panel panel_RemainingCP;
        private System.Windows.Forms.Panel panel_RemainingLostGames;
        private System.Windows.Forms.Label label_RemainingLostGamesIndicator;
        private System.Windows.Forms.Label label_RemainingLostGames;
        private System.Windows.Forms.Panel panel_RemainingWonGames;
        private System.Windows.Forms.Label label_RemainingWonGamesIndicator;
        private System.Windows.Forms.Label label_RemainingWonGames;
        private System.Windows.Forms.Panel panel_RemainingAvgGames;
        private System.Windows.Forms.Label label_RemainingAvgGamesIndicator;
        private System.Windows.Forms.Label label_RemainingAvgGames;
        private System.Windows.Forms.Panel panel_ActionButtons;
        private System.Windows.Forms.Label label_MasteryLevelIndicator;
        private System.Windows.Forms.Button button_ViewMatches;
        private System.Windows.Forms.Button button_AddMatch;
        private System.Windows.Forms.Label label_ChampMatchesIndicator;
        private System.Windows.Forms.Panel panel_ChampMatches;
        private System.Windows.Forms.Label label_ChampMatches;
        private System.Windows.Forms.Label label_ChampWRIndicator;
        private System.Windows.Forms.Label label_ChampWR;
        private System.Windows.Forms.Label label_ChampWonMatchesIndicator;
        private System.Windows.Forms.Label label_ChampWonMatches;
        private System.Windows.Forms.Label label_KDAIndicator;
        private System.Windows.Forms.ComboBox combo_PickAChamp;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addAChampionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private MainDBDataSet mainDBDataSet;
        private System.Windows.Forms.BindingSource championsBindingSource;
        private MainDBDataSetTableAdapters.ChampionsTableAdapter championsTableAdapter;
    }
}

