﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lol_Mastery_Tracker
{
    class Champion
    {
        public int Id { get; private set; }
        public string Name { get; private set; }

        public Champion(string dataSource)
        {
            string[] data = dataSource.Split(',');

            this.Id = int.Parse(data[0]);
            this.Name = data[1];
        }

        public string GenerateRegistry()
        {
            return $"{Id},{Name}";
        }
    }
}
