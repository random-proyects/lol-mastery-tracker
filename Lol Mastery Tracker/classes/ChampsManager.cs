﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lol_Mastery_Tracker
{
    class ChampsManager
    {
        string FileName = $"/Champion.s";
        public int Count { get; private set; }
        public int[] Masteries { get; } = { 1800, 6000, 12600, 21600 };
        public void Add(Champion champ)
        {
            FileStream fs = new FileStream(FileName, FileMode.Append, FileAccess.Write);
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.WriteLine(champ.GenerateRegistry());
            }
            fs.Close();
        }

        public void Delete(int id)
        {
            string output = string.Empty;
            FileStream fs = new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.Read);
            using (StreamReader reader = new StreamReader(fs))
            {
                string line = reader.ReadLine();
                while (line != null)
                {
                    Champion current = new Champion(line);
                    if (current.Id != id)
                    {
                        output += line + Environment.NewLine;
                    }
                    line = reader.ReadLine();
                }
            }
            fs.Close();
            fs = new FileStream(FileName, FileMode.Truncate, FileAccess.Write);
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(output);
            }
            fs.Close();
        }

        public List<Champion> List()
        {
            Count = 0;
            List<Champion> list = new List<Champion>();
            FileStream fs = new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.Read);
            using (StreamReader reader = new StreamReader(fs))
            {
                string line = reader.ReadLine();
                while (line != null)
                {
                    Count++;
                    Champion current = new Champion(line);
                    list.Add(current);
                    line = reader.ReadLine();
                }
            }
            fs.Close();
            return list;
        }
    }
}
