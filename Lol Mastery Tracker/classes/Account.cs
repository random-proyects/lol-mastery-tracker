﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lol_Mastery_Tracker
{
    class Account
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public float KDA { get; set; }
        public float Kills { get; set; }
        public float Deaths { get; set; }
        public float Assists { get; set; }
        public int TotalGames { get; set; }
        public int WonGames { get; set; }
        public int LostGames { get; set; }
        public float Winrate { get; set; }
        public int OwnedChamps { get; set; }
        public float TotalCP { get; set; }
        public int AppLanguage { get; set; }

        public Account(string dataSource)
        {
            string[] data = dataSource.Split(',');

            this.Id = int.Parse(data[0]);
            this.Name = data[1];
            this.KDA = float.Parse(data[2]);
            this.Kills = float.Parse(data[3]);
            this.Deaths = float.Parse(data[4]);
            this.Assists = float.Parse(data[5]);
            this.TotalGames = int.Parse(data[6]);
            this.WonGames = int.Parse(data[7]);
            this.LostGames = int.Parse(data[8]);
            this.Winrate = float.Parse(data[9]);
            this.OwnedChamps = int.Parse(data[10]);
            this.TotalCP = float.Parse(data[11]);
            this.AppLanguage = int.Parse(data[12]);
        }

        public string GenerateRegistry()
        {
            return $"{Id},{Name}";
        }
    }

}
