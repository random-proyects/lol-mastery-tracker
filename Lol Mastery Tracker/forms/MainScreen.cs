﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Lol_Mastery_Tracker
{
    public partial class MainScreen : Form
    {
        
        ChampsManager Champs = new ChampsManager();

        public MainScreen()
        {
            InitializeComponent();
            this.combo_PickAChamp.DataSource = Champs.List();
        }

        private void MainScreen_Load(object sender, EventArgs e)
        {

        }

        private void strip_NewSummoner_Click(object sender, EventArgs e)
        {
            var form = new NewSummonerScreen();
            form.Location = this.Location;
            form.StartPosition = FormStartPosition.Manual;
            form.FormClosing += delegate { this.Show(); };
            form.ShowDialog();
        }

        private void combo_PickAChamp_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
