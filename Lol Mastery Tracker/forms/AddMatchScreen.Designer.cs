﻿
namespace Lol_Mastery_Tracker
{
    partial class AddMatchScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.group_MatchInfo = new System.Windows.Forms.GroupBox();
            this.label_GameType = new System.Windows.Forms.Label();
            this.group_MatchInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // group_MatchInfo
            // 
            this.group_MatchInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.group_MatchInfo.CausesValidation = false;
            this.group_MatchInfo.Controls.Add(this.label_GameType);
            this.group_MatchInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.group_MatchInfo.Font = new System.Drawing.Font("Friz Quadrata Std", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group_MatchInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(210)))), ((int)(((byte)(139)))));
            this.group_MatchInfo.Location = new System.Drawing.Point(12, 12);
            this.group_MatchInfo.Name = "group_MatchInfo";
            this.group_MatchInfo.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.group_MatchInfo.Size = new System.Drawing.Size(448, 191);
            this.group_MatchInfo.TabIndex = 10;
            this.group_MatchInfo.TabStop = false;
            this.group_MatchInfo.Text = "Match Info";
            this.group_MatchInfo.Enter += new System.EventHandler(this.group_MatchInfo_Enter);
            // 
            // label_GameType
            // 
            this.label_GameType.AutoSize = true;
            this.label_GameType.Font = new System.Drawing.Font("Friz Quadrata Std", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_GameType.Location = new System.Drawing.Point(6, 26);
            this.label_GameType.Name = "label_GameType";
            this.label_GameType.Size = new System.Drawing.Size(40, 18);
            this.label_GameType.TabIndex = 10;
            this.label_GameType.Text = "Type";
            // 
            // AddMatchScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(471, 365);
            this.Controls.Add(this.group_MatchInfo);
            this.Name = "AddMatchScreen";
            this.Text = "Add New Match";
            this.Load += new System.EventHandler(this.AddMatchScreen_Load);
            this.group_MatchInfo.ResumeLayout(false);
            this.group_MatchInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox group_MatchInfo;
        private System.Windows.Forms.Label label_GameType;
    }
}