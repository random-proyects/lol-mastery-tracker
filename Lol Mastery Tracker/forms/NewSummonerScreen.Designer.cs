﻿
namespace Lol_Mastery_Tracker
{
    partial class NewSummonerScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.group_SummonerInfo = new System.Windows.Forms.GroupBox();
            this.panel_Buttons = new System.Windows.Forms.Panel();
            this.buttonNext = new System.Windows.Forms.Button();
            this.button_Cancel = new System.Windows.Forms.Button();
            this.group_OwnedChampsSelect = new System.Windows.Forms.GroupBox();
            this.checkedListBox_OwnedChampsSelect = new System.Windows.Forms.CheckedListBox();
            this.groupBox_Name = new System.Windows.Forms.GroupBox();
            this.textBox_Name = new System.Windows.Forms.TextBox();
            this.group_SummonerInfo.SuspendLayout();
            this.panel_Buttons.SuspendLayout();
            this.group_OwnedChampsSelect.SuspendLayout();
            this.groupBox_Name.SuspendLayout();
            this.SuspendLayout();
            // 
            // group_SummonerInfo
            // 
            this.group_SummonerInfo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.group_SummonerInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(20)))), ((int)(((byte)(40)))));
            this.group_SummonerInfo.CausesValidation = false;
            this.group_SummonerInfo.Controls.Add(this.panel_Buttons);
            this.group_SummonerInfo.Controls.Add(this.group_OwnedChampsSelect);
            this.group_SummonerInfo.Controls.Add(this.groupBox_Name);
            this.group_SummonerInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.group_SummonerInfo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.group_SummonerInfo.Font = new System.Drawing.Font("Friz Quadrata Std", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.group_SummonerInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(210)))), ((int)(((byte)(139)))));
            this.group_SummonerInfo.Location = new System.Drawing.Point(0, 0);
            this.group_SummonerInfo.Name = "group_SummonerInfo";
            this.group_SummonerInfo.Size = new System.Drawing.Size(164, 278);
            this.group_SummonerInfo.TabIndex = 11;
            this.group_SummonerInfo.TabStop = false;
            this.group_SummonerInfo.Text = "Account Info";
            // 
            // panel_Buttons
            // 
            this.panel_Buttons.Controls.Add(this.buttonNext);
            this.panel_Buttons.Controls.Add(this.button_Cancel);
            this.panel_Buttons.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_Buttons.Location = new System.Drawing.Point(3, 241);
            this.panel_Buttons.Name = "panel_Buttons";
            this.panel_Buttons.Size = new System.Drawing.Size(158, 30);
            this.panel_Buttons.TabIndex = 12;
            // 
            // buttonNext
            // 
            this.buttonNext.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonNext.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(40)))));
            this.buttonNext.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonNext.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(196)))), ((int)(((byte)(237)))));
            this.buttonNext.FlatAppearance.BorderSize = 2;
            this.buttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNext.Font = new System.Drawing.Font("Friz Quadrata Std", 12F);
            this.buttonNext.Location = new System.Drawing.Point(83, 0);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(75, 30);
            this.buttonNext.TabIndex = 0;
            this.buttonNext.Text = "Next";
            this.buttonNext.UseVisualStyleBackColor = false;
            // 
            // button_Cancel
            // 
            this.button_Cancel.AutoSize = true;
            this.button_Cancel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(40)))));
            this.button_Cancel.Dock = System.Windows.Forms.DockStyle.Left;
            this.button_Cancel.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(196)))), ((int)(((byte)(237)))));
            this.button_Cancel.FlatAppearance.BorderSize = 2;
            this.button_Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Cancel.Font = new System.Drawing.Font("Friz Quadrata Std", 12F);
            this.button_Cancel.Location = new System.Drawing.Point(0, 0);
            this.button_Cancel.Name = "button_Cancel";
            this.button_Cancel.Size = new System.Drawing.Size(75, 30);
            this.button_Cancel.TabIndex = 0;
            this.button_Cancel.Text = "Cancel";
            this.button_Cancel.UseVisualStyleBackColor = false;
            // 
            // group_OwnedChampsSelect
            // 
            this.group_OwnedChampsSelect.Controls.Add(this.checkedListBox_OwnedChampsSelect);
            this.group_OwnedChampsSelect.Dock = System.Windows.Forms.DockStyle.Top;
            this.group_OwnedChampsSelect.Font = new System.Drawing.Font("Friz Quadrata Std", 12F);
            this.group_OwnedChampsSelect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(210)))), ((int)(((byte)(139)))));
            this.group_OwnedChampsSelect.Location = new System.Drawing.Point(3, 78);
            this.group_OwnedChampsSelect.Name = "group_OwnedChampsSelect";
            this.group_OwnedChampsSelect.Size = new System.Drawing.Size(158, 163);
            this.group_OwnedChampsSelect.TabIndex = 11;
            this.group_OwnedChampsSelect.TabStop = false;
            this.group_OwnedChampsSelect.Text = "Owned champions:";
            // 
            // checkedListBox_OwnedChampsSelect
            // 
            this.checkedListBox_OwnedChampsSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(40)))));
            this.checkedListBox_OwnedChampsSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkedListBox_OwnedChampsSelect.Font = new System.Drawing.Font("Friz Quadrata Std", 11F);
            this.checkedListBox_OwnedChampsSelect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(210)))), ((int)(((byte)(139)))));
            this.checkedListBox_OwnedChampsSelect.FormattingEnabled = true;
            this.checkedListBox_OwnedChampsSelect.Location = new System.Drawing.Point(3, 23);
            this.checkedListBox_OwnedChampsSelect.Name = "checkedListBox_OwnedChampsSelect";
            this.checkedListBox_OwnedChampsSelect.ScrollAlwaysVisible = true;
            this.checkedListBox_OwnedChampsSelect.Size = new System.Drawing.Size(152, 137);
            this.checkedListBox_OwnedChampsSelect.TabIndex = 0;
            this.checkedListBox_OwnedChampsSelect.SelectedIndexChanged += new System.EventHandler(this.checkedListBox_OwnedChampsSelect_SelectedIndexChanged);
            this.checkedListBox_OwnedChampsSelect.Enter += new System.EventHandler(this.checkedListBox_OwnedChampsSelect_Enter);
            // 
            // groupBox_Name
            // 
            this.groupBox_Name.Controls.Add(this.textBox_Name);
            this.groupBox_Name.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox_Name.Font = new System.Drawing.Font("Friz Quadrata Std", 12F);
            this.groupBox_Name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(210)))), ((int)(((byte)(139)))));
            this.groupBox_Name.Location = new System.Drawing.Point(3, 26);
            this.groupBox_Name.Name = "groupBox_Name";
            this.groupBox_Name.Size = new System.Drawing.Size(158, 52);
            this.groupBox_Name.TabIndex = 11;
            this.groupBox_Name.TabStop = false;
            this.groupBox_Name.Text = "Summoner Name";
            // 
            // textBox_Name
            // 
            this.textBox_Name.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(40)))));
            this.textBox_Name.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox_Name.Font = new System.Drawing.Font("Friz Quadrata Std", 11F);
            this.textBox_Name.ForeColor = System.Drawing.Color.White;
            this.textBox_Name.Location = new System.Drawing.Point(3, 23);
            this.textBox_Name.Name = "textBox_Name";
            this.textBox_Name.Size = new System.Drawing.Size(152, 25);
            this.textBox_Name.TabIndex = 1;
            // 
            // NewSummonerScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(35)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(164, 275);
            this.Controls.Add(this.group_SummonerInfo);
            this.Name = "NewSummonerScreen";
            this.Text = "Add a New Summoner";
            this.group_SummonerInfo.ResumeLayout(false);
            this.panel_Buttons.ResumeLayout(false);
            this.panel_Buttons.PerformLayout();
            this.group_OwnedChampsSelect.ResumeLayout(false);
            this.groupBox_Name.ResumeLayout(false);
            this.groupBox_Name.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox group_SummonerInfo;
        private System.Windows.Forms.TextBox textBox_Name;
        private System.Windows.Forms.GroupBox group_OwnedChampsSelect;
        private System.Windows.Forms.CheckedListBox checkedListBox_OwnedChampsSelect;
        private System.Windows.Forms.Panel panel_Buttons;
        private System.Windows.Forms.Button button_Cancel;
        private System.Windows.Forms.Button buttonNext;
        private System.Windows.Forms.GroupBox groupBox_Name;
    }
}