﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lol_Mastery_Tracker
{
    public partial class NewSummonerScreen : Form
    {
        ChampsManager Champs = new ChampsManager();

        public NewSummonerScreen()
        {
            InitializeComponent();
        }

        private void checkedListBox_OwnedChampsSelect_Enter(object sender, EventArgs e)
        {
            checkedListBox_OwnedChampsSelect.Items.Clear();
            for (int i=0; i<Champs.List().Count; i++)
            {
                checkedListBox_OwnedChampsSelect.Items.Add(Champs.List()[i].Name);
            }
        }

        private void checkedListBox_OwnedChampsSelect_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
